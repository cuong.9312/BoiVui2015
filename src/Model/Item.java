package Model;

import java.io.Serializable;

public class Item implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String img;
	String name;
	int idFile;

	public Item() {
		// TODO Auto-generated constructor stub
	}

	public Item(String img, String name, int idFile) {
		super();
		this.img = img;
		this.name = name;
		this.idFile = idFile;
	}

	public Item(String img, String name) {
		super();
		this.img = img;
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIdFile() {
		return idFile;
	}

	public void setIdFile(int idFile) {
		this.idFile = idFile;
	}

	@Override
	public String toString() {
		return name;
	}

	

}
