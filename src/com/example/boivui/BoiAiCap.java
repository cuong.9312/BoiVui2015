package com.example.boivui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BoiAiCap extends Activity {
	EditText editTenBan;
	TextView tViewTenBan;
	TextView tViewKetQua;
	Button btnBoiTen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_boi_ai_cap);
		getControls();
		inits();
		addEvents();
	}

	public void getControls() {
		editTenBan = (EditText) findViewById(R.id.editTenBan);
		tViewTenBan = (TextView) findViewById(R.id.tViewTenBan);
		tViewKetQua = (TextView) findViewById(R.id.tViewKetQua);
		btnBoiTen = (Button) findViewById(R.id.btnBoiTen);
	}

	public void inits() {
		setFontAndActionBar();
	}

	public void addEvents() {
		btnBoiTen.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String ten = editTenBan.getText().toString() + "";
				if (ten.equals("")) {
					Toast.makeText(getBaseContext(), "Không để tên trống",
							Toast.LENGTH_SHORT).show();
				} else {
					String ketqua = "";
					int idFile = 0;
					switch (tenSangQueBoi(ten)) {
					case 1:
						idFile = R.drawable.boiten1;
						break;
					case 2:
						idFile = R.drawable.boiten2;
						break;
					case 3:
						idFile = R.drawable.boiten3;
						break;
					case 4:
						idFile = R.drawable.boiten4;
						break;
					case 5:
						idFile = R.drawable.boiten5;
						break;
					case 6:
						idFile = R.drawable.boiten6;
						break;
					case 7:
						idFile = R.drawable.boiten7;
						break;
					case 8:
						idFile = R.drawable.boiten8;
						break;
					case 9:
						idFile = R.drawable.boiten9;
						break;
					default:
						idFile = 0;
						break;
					}
					ketqua = readFile(idFile);
					tViewKetQua.setText(ketqua);
				}

			}
		});
	}

	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("Bói Tên");
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
		tViewTenBan.setTypeface(custom_font);
		editTenBan.setTypeface(custom_font);
	}

	public String readFile(int idFile) {
		String result = "";
		String data;
		InputStream in = getResources().openRawResource(idFile);
		BufferedReader bufreader = new BufferedReader(new InputStreamReader(in));
		if (in != null) {
			try {
				while ((data = bufreader.readLine()) != null) {
					result += data + "\n";
				}
				in.close();
			} catch (IOException ex) {
				Log.e("ERROR", ex.getMessage());
			}

		}
		return result;
	}

	public static int tongCacChuSo(int input) {
		int ketqua = 0;
		while (input > 0) {
			ketqua = ketqua + input % 10;
			input = input / 10;
		}
		return ketqua;
	}

	public static int toiUu(int input) {
		int ketqua = tongCacChuSo(input);
		while (ketqua >= 10) {
			ketqua = tongCacChuSo(ketqua);
		}
		return ketqua;
	}

	public static int tenSangQueBoi(String ten) {
		ten = ten.replaceAll(" ", "");

		int ketqua = 0;
		for (int i = 0; i < ten.length(); i++) {
			if (ten.charAt(i) == 'a' || ten.charAt(i) == 'j'
					|| ten.charAt(i) == 's') {
				ketqua = ketqua + 1;
			}
			if (ten.charAt(i) == 'b' || ten.charAt(i) == 'k'
					|| ten.charAt(i) == 't') {
				ketqua = ketqua + 2;
			}
			if (ten.charAt(i) == 'c' || ten.charAt(i) == 'l'
					|| ten.charAt(i) == 'u') {
				ketqua = ketqua + 3;
			}
			if (ten.charAt(i) == 'd' || ten.charAt(i) == 'm'
					|| ten.charAt(i) == 'v') {
				ketqua = ketqua + 4;
			}
			if (ten.charAt(i) == 'e' || ten.charAt(i) == 'n'
					|| ten.charAt(i) == 'w') {
				ketqua = ketqua + 5;
			}
			if (ten.charAt(i) == 'f' || ten.charAt(i) == 'o'
					|| ten.charAt(i) == 'x') {
				ketqua = ketqua + 6;
			}
			if (ten.charAt(i) == 'g' || ten.charAt(i) == 'p'
					|| ten.charAt(i) == 'y') {
				ketqua = ketqua + 7;
			}
			if (ten.charAt(i) == 'h' || ten.charAt(i) == 'q'
					|| ten.charAt(i) == 'z') {
				ketqua = ketqua + 8;
			}
			if (ten.charAt(i) == 'y' || ten.charAt(i) == 'r') {
				ketqua = ketqua + 9;
			}
		}
		return toiUu(ketqua);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.boi_ai_cap, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case 0:
			Toast.makeText(this, "You clicked Setting item", Toast.LENGTH_SHORT)
					.show();
			return true;
		}
		return false;
	}
}
