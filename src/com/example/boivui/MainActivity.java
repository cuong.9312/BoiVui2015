package com.example.boivui;

import java.util.ArrayList;

import Custom.MyArrayAdapter;
import Model.Item;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	ListView listViewItem;
	ArrayList<Item> arr = new ArrayList<Item>();
	MyArrayAdapter adapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getControls();
		inits();
		addEvents();
	}

	public void getControls() {
		listViewItem = (ListView) findViewById(R.id.listViewItem);
	}

	public void inits() {
		setFontAndActionBar();

		arr = new ArrayList<Item>();
		arr.add(new Item("cunghoangdao", "12 Cung hoàng đạo"));
		arr.add(new Item("congiap", "12 Con giáp"));
		arr.add(new Item("boingaysinh", "Bói theo ngày sinh"));
		arr.add(new Item("boitenaicap", "Bói tên Ai Cập"));
		arr.add(new Item("gioithieu", "Giới thiệu phần mềm"));
		adapter = new MyArrayAdapter(this, R.layout.custom_listview, arr);
		listViewItem.setAdapter(adapter);
	}

	public void addEvents() {
		listViewItem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Item item = arr.get(position);
				if (item.getImg().equalsIgnoreCase("cunghoangdao")) {
					Intent intent = new Intent(getBaseContext(),
							List12CungHoangDao.class);
					startActivity(intent);
					return;
				}
				if (item.getImg().equalsIgnoreCase("congiap")) {
					Intent intent = new Intent(getBaseContext(),
							List12ConGiap.class);
					startActivity(intent);
					return;
				}
				
				if (item.getImg().equalsIgnoreCase("boingaysinh")) {
					Intent intent = new Intent(getBaseContext(),
							BoiNgaySinh.class);
					startActivity(intent);
					return;
				}
				if (item.getImg().equalsIgnoreCase("boitenaicap")) {
					Intent intent = new Intent(getBaseContext(),
							BoiAiCap.class);
					startActivity(intent);
					return;
				}
				if (item.getImg().equalsIgnoreCase("gioithieu")) {
					Intent intent = new Intent(getBaseContext(),
							GioiThieu.class);
					startActivity(intent);
					return;
				}

			}
		});
	}


	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		// actionBar.setIcon(R.drawable.m1);
		// actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("Bói vui 2015");
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case 0:
			Toast.makeText(this, "You clicked Setting item", Toast.LENGTH_SHORT)
					.show();
			return true;
		}
		return false;
	}
}
