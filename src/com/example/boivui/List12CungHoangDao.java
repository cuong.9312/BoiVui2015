package com.example.boivui;

import java.util.ArrayList;

import Custom.MyArrayAdapter;
import Model.Item;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class List12CungHoangDao extends Activity {
	ListView listViewItem;
	ArrayList<Item> arr = new ArrayList<Item>();
	MyArrayAdapter adapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list12_cung_hoang_dao);
		getControls();
		inits();
		addEvents();
	}

	public void getControls() {
		listViewItem = (ListView) findViewById(R.id.listViewItem);
	}

	public void inits() {
		setFontAndActionBar();

		arr = new ArrayList<Item>();
		arr.add(new Item("bachduong", "Bạch dương", R.drawable.bachduong1));
		arr.add(new Item("kimnguu", "Kim ngưu", R.drawable.kimnguu1));
		arr.add(new Item("songtu", "Song tử", R.drawable.songtu1));
		arr.add(new Item("cugiai", "Cự giải", R.drawable.cugiai1));
		arr.add(new Item("sutu", "Sư tử", R.drawable.sutu1));
		arr.add(new Item("xunu", "Xử nữ", R.drawable.xunu1));
		arr.add(new Item("thienbinh", "Thiên bình", R.drawable.thienbinh1));
		arr.add(new Item("hocap", "Hổ cáp", R.drawable.hocap1));
		arr.add(new Item("nhanma", "Nhân mã", R.drawable.nhanma1));
		arr.add(new Item("maket", "Ma kết", R.drawable.maket1));
		arr.add(new Item("baobinh", "Bảo bình", R.drawable.baobinh1));
		arr.add(new Item("songngu", "Song ngư", R.drawable.songngu1));

		adapter = new MyArrayAdapter(this, R.layout.custom_listview, arr);
		listViewItem.setAdapter(adapter);
	}

	public void addEvents() {
		listViewItem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Item item = arr.get(position);
				Intent intent = new Intent(getBaseContext(),
						PageView.class);
				intent.putExtra("item", item);
				startActivity(intent);

			}
		});
	}

	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("12 cung hoàng đạo");
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list12_cung_hoang_dao, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case 0:
			Toast.makeText(this, "You clicked Setting item", Toast.LENGTH_SHORT)
					.show();
			return true;
		}
		return false;
	}
}
