package com.example.boivui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BoiNgaySinh extends Activity {
	EditText editNgay;
	EditText editThang;
	EditText editNam;
	Button btnBoiNgaySinh;
	String result = "";
	TextView tViewKetQua;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_boi_ngay_sinh);
		getControls();
		inits();
		addEvents();
	}

	public void getControls() {
		editNgay = (EditText) findViewById(R.id.editNgay);
		editThang = (EditText) findViewById(R.id.editThang);
		editNam = (EditText) findViewById(R.id.editNam);
		btnBoiNgaySinh = (Button) findViewById(R.id.btnBoiNgaySinh);
		tViewKetQua = (TextView) findViewById(R.id.tViewKetQua);
	}

	public void inits() {
		setFontAndActionBar();
	}

	public void addEvents() {
		btnBoiNgaySinh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String ngay = editNgay.getText().toString() + "";
				String thang = editThang.getText().toString() + "";
				String nam = editNam.getText().toString() + "";
				String ngaySinh = ngay + thang + nam;
				if (ngay.equals("") || thang.equals("") || nam.equals("")) {
					Toast.makeText(getBaseContext(),
							"nhập ngày tháng năm đầy đủ", Toast.LENGTH_SHORT)
							.show();
				} else {
					String ketqua = "";
					int idFile = 0;
					switch (ngaySinhSangQueBoi(ngaySinh)) {
					case 1:
						idFile = R.drawable.boingaysinh1;
						break;
					case 2:
						idFile = R.drawable.boingaysinh2;
						break;
					case 3:
						idFile = R.drawable.boingaysinh3;
						break;
					case 4:
						idFile = R.drawable.boingaysinh4;
						break;
					case 5:
						idFile = R.drawable.boingaysinh5;
						break;
					case 6:
						idFile = R.drawable.boingaysinh6;
						break;
					case 7:
						idFile = R.drawable.boingaysinh7;
						break;
					case 8:
						idFile = R.drawable.boingaysinh8;
						break;
					case 9:
						idFile = R.drawable.boingaysinh9;
						break;
					case 11:
						idFile = R.drawable.boingaysinh11;
						break;
					case 22:
						idFile = R.drawable.boingaysinh22;
						break;
					case 33:
						idFile = R.drawable.boingaysinh33;
						break;
					default:
						idFile = 0;
						break;
					}
					ketqua = readFile(idFile);
					tViewKetQua.setText(ketqua);
				}

			}
		});
	}

	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("Bói ngày sinh");
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
	}

	public String readFile(int idFile) {
		String result = "";
		String data;
		InputStream in = getResources().openRawResource(idFile);
		BufferedReader bufreader = new BufferedReader(new InputStreamReader(in));
		if (in != null) {
			try {
				while ((data = bufreader.readLine()) != null) {
					result += data + "\n";
				}
				in.close();
			} catch (IOException ex) {
				Log.e("ERROR", ex.getMessage());
			}

		}
		return result;
	}

	public static int tongCacChuSo(int input) {
		int ketqua = 0;
		while (input > 0) {
			ketqua = ketqua + input % 10;
			input = input / 10;
		}
		return ketqua;
	}

	public static int toiUuNgaySinh(int input) {
		int ketqua = tongCacChuSo(input);
		while (ketqua >= 10 && ketqua != 11 && ketqua != 22 && ketqua != 33) {
			ketqua = tongCacChuSo(ketqua);
		}
		return ketqua;
	}

	public static int ngaySinhSangQueBoi(String ngaySinh) {
		int ketqua = 0;
		for (int i = 0; i < ngaySinh.length(); i++) {
			int k = Integer.parseInt(ngaySinh.substring(i, i + 1));
			// System.out.println(k+"   ");
			ketqua = ketqua+k;
		}
		return toiUuNgaySinh(ketqua);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.boi_ngay_sinh, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case 0:
			Toast.makeText(this, "You clicked Setting item", Toast.LENGTH_SHORT)
					.show();
			return true;
		}
		return false;
	}

}
