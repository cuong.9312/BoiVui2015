package com.example.boivui;

import java.util.ArrayList;

import Custom.MyArrayAdapter;
import Model.Item;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class List12ConGiap extends Activity {
	ListView listViewItem;
	ArrayList<Item> arr = new ArrayList<Item>();
	MyArrayAdapter adapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list12_con_giap);
		getControls();
		inits();
		addEvents();
	}

	public void getControls() {
		listViewItem = (ListView) findViewById(R.id.listViewItem);
	}

	public void inits() {
		setFontAndActionBar();

		arr = new ArrayList<Item>();
		arr.add(new Item("ti", "Tý", R.drawable.ti1));
		arr.add(new Item("suu", "Sửu", R.drawable.suu1));
		arr.add(new Item("dan", "Dần", R.drawable.dan1));
		arr.add(new Item("mao", "Mão", R.drawable.mao1));
		arr.add(new Item("thin", "Thìn", R.drawable.thin1));
		arr.add(new Item("ty", "Tỵ", R.drawable.ty1));
		arr.add(new Item("ngo", "Ngọ", R.drawable.ngo1));
		arr.add(new Item("mui", "Mùi", R.drawable.mui1));
		arr.add(new Item("hoi", "Hợi", R.drawable.hoi1));
		arr.add(new Item("dau", "Dậu", R.drawable.dau1));
		arr.add(new Item("than", "Thân", R.drawable.than1));
		arr.add(new Item("tuat", "Tuất", R.drawable.tuat1));

		adapter = new MyArrayAdapter(this, R.layout.custom_listview, arr);
		listViewItem.setAdapter(adapter);
	}

	public void addEvents() {
		listViewItem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Item item = arr.get(position);
				Intent intent = new Intent(getBaseContext(),
						PageView.class);
				intent.putExtra("item", item);
				startActivity(intent);

			}
		});
	}

	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("12 con giáp");
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list12_con_giap, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case 0:
			Toast.makeText(this, "You clicked Setting item", Toast.LENGTH_SHORT)
					.show();
			return true;
		}
		return false;
	}
}
