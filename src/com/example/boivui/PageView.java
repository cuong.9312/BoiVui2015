package com.example.boivui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import Model.Item;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class PageView extends Activity {

	TextView tView;
	Item item;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_page_view);
		getControls();
		inits();
	}

	public void getControls() {
		tView = (TextView) findViewById(R.id.tView);
	}

	public void inits() {
		Intent intent = getIntent();
		item = (Item) intent.getSerializableExtra("item");
		tView.setText(readFile());
		setFontAndActionBar();
	}

	public String readFile() {
		String result = "";
		String data;
		InputStream in = getResources().openRawResource(item.getIdFile());
		BufferedReader bufreader = new BufferedReader(new InputStreamReader(in));
		if (in != null) {
			try {
				while ((data = bufreader.readLine()) != null) {
					result += data + "\n";
				}
				in.close();
			} catch (IOException ex) {
				Log.e("ERROR", ex.getMessage());
			}

		}
		return result;
	}

	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(item.getName());
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
		tView.setTypeface(custom_font);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.page_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case 0:
			Toast.makeText(this, "You clicked Setting item", Toast.LENGTH_SHORT)
					.show();
			return true;
		}
		return false;
	}
}
