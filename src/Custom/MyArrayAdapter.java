package Custom;

import java.util.ArrayList;

import Model.Item;
import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.boivui.R;


public class MyArrayAdapter extends ArrayAdapter<Item> {
	Activity context = null;
	ArrayList<Item> arr = null;
	int layoutId;

	public MyArrayAdapter(Activity context, int layoutId, ArrayList<Item> arr) {
		super(context, layoutId, arr);
		this.context = context;
		this.layoutId = layoutId;
		this.arr = arr;

		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		convertView = inflater.inflate(layoutId, null);
		if (arr.size() > 0 && position >= 0) {
			final Item item = arr.get(position);
			final TextView txtdisplay = (TextView) convertView
					.findViewById(R.id.txtitem);
			final String name = arr.get(position).toString();
			txtdisplay.setText(name);
			Typeface custom_font = Typeface.createFromAsset(txtdisplay
					.getContext().getAssets(), "fonts/Tabitha.ttf");
			txtdisplay.setTypeface(custom_font);
			final ImageView imgitem = (ImageView) convertView
					.findViewById(R.id.imgitem);
			if (item.getImg().equalsIgnoreCase("cunghoangdao"))
				imgitem.setImageResource(R.drawable.chomsao);
			if (item.getImg().equalsIgnoreCase("congiap"))
				imgitem.setImageResource(R.drawable.congiap);
			if (item.getImg().equalsIgnoreCase("boitenaicap"))
				imgitem.setImageResource(R.drawable.boiten);
			if (item.getImg().equalsIgnoreCase("boingaysinh"))
				imgitem.setImageResource(R.drawable.boingaysinh);
			if (item.getImg().equalsIgnoreCase("gioithieu"))
				imgitem.setImageResource(R.drawable.gioithieu);
			
			
			
			if (item.getImg().equalsIgnoreCase("bachduong"))
				imgitem.setImageResource(R.drawable.bachduong);
			if (item.getImg().equalsIgnoreCase("kimnguu"))
				imgitem.setImageResource(R.drawable.kimnguu);
			if (item.getImg().equalsIgnoreCase("songtu"))
				imgitem.setImageResource(R.drawable.songtu);
			if (item.getImg().equalsIgnoreCase("cugiai"))
				imgitem.setImageResource(R.drawable.cugiai);
			if (item.getImg().equalsIgnoreCase("sutu"))
				imgitem.setImageResource(R.drawable.sutu);
			if (item.getImg().equalsIgnoreCase("xunu"))
				imgitem.setImageResource(R.drawable.xunu);
			if (item.getImg().equalsIgnoreCase("thienbinh"))
				imgitem.setImageResource(R.drawable.thienbinh);
			if (item.getImg().equalsIgnoreCase("hocap"))
				imgitem.setImageResource(R.drawable.hocap);
			if (item.getImg().equalsIgnoreCase("nhanma"))
				imgitem.setImageResource(R.drawable.nhanma);
			if (item.getImg().equalsIgnoreCase("maket"))
				imgitem.setImageResource(R.drawable.maket);
			if (item.getImg().equalsIgnoreCase("baobinh"))
				imgitem.setImageResource(R.drawable.baobinh);
			if (item.getImg().equalsIgnoreCase("songngu"))
				imgitem.setImageResource(R.drawable.songngu);
			if (item.getImg().equalsIgnoreCase("ti"))
				imgitem.setImageResource(R.drawable.ti);
			if (item.getImg().equalsIgnoreCase("suu"))
				imgitem.setImageResource(R.drawable.suu);
			if (item.getImg().equalsIgnoreCase("dan"))
				imgitem.setImageResource(R.drawable.dan);
			if (item.getImg().equalsIgnoreCase("mao"))
				imgitem.setImageResource(R.drawable.mao);
			if (item.getImg().equalsIgnoreCase("thin"))
				imgitem.setImageResource(R.drawable.thin);
			if (item.getImg().equalsIgnoreCase("ty"))
				imgitem.setImageResource(R.drawable.ty);
			if (item.getImg().equalsIgnoreCase("ngo"))
				imgitem.setImageResource(R.drawable.ngo);
			if (item.getImg().equalsIgnoreCase("mui"))
				imgitem.setImageResource(R.drawable.mui);
			if (item.getImg().equalsIgnoreCase("dau"))
				imgitem.setImageResource(R.drawable.dau);
			if (item.getImg().equalsIgnoreCase("hoi"))
				imgitem.setImageResource(R.drawable.hoi);
			if (item.getImg().equalsIgnoreCase("than"))
				imgitem.setImageResource(R.drawable.than);
			if (item.getImg().equalsIgnoreCase("tuat"))
				imgitem.setImageResource(R.drawable.tuat);
		}
		return convertView;
	}
}
