﻿ Số 7

Số 7 là những người sống nội tâm, họ có linh cảm thiên phú. Những ước mơ của họ thuộc về một thế giới khác. Họ luôn tìm kiếm chân lý, lòng ham mê học hỏi của họ là không có giới hạn.

Trí lực dồi dào và sự hoài nghi đối với tất cả những gì đã được công nhận là đặc điểm của người số 7. Họ là những triết gia thực thụ, họ cống hiến cho lý tưởng và luôn tìm tòi những cách giải quyết vấn đề tốt hơn. Để có thể tin được vào điều gì đó, trước hết họ phải tìm hiểu rất kỹ. Họ không đồng tình với số đông, không tin vào dư luận, họ là những người cá nhân chủ nghĩa. Họ yêu tự do. Tiền bạc, danh tiếng hay sự thăng tiến không có ý nghĩa lớn đối với họ.

Số 7 giải quyết các vấn đề khoa học tốt hơn xử lý tình cảm. Họ thường không có nhiều bạn, trong sâu thẳm tâm hồn họ là những người cô đơn. Đôi khi họ thấy tự ti về bề ngòai của mình. Họ không muốn nhờ người khác giúp đỡ khi gặp khó khăn. Mọi người thường không hiểu được các vấn đề của họ. Họ không thích thổ lộ tình cảm và thường tỏ ra kiêu hãnh.

Đối với một số người thì số 7 là những người kiêu căng và khó gần. Họ dễ bị tổn thương. Họ đề cao lòng tự trọng và danh dự. Họ cầu tòan và do đó thường đặt ra cho mình những mục tiêu khó đạt. Đôi khi họ bi quan và buồn bã. Để có thể vui sống những người số 7 nên biết nói ra cảm xúc của mình.

Số 7 say mê tất cả những gì bí ẩn. Họ chọn những người bạn cùng chung sở thích. Họ ghét sự tục tằn và hạ lưu.

Trong công việc họ rất tận tụy và có trách nhiệm, nhưng họ rất ghét thực hiện những chỉ thị ngu ngốc và sẵn sàng chỉ trích sếp. Họ thích hợp với những nghề tự do. Họ hợp với việc nghiên cứu khoa học hoặc giảng dạy. Họ là những kỹ sư, thợ kim hoàn và nhũng nhà thực vật học tốt.

Trong tình yêu họ hay gặp khó khăn vì quá xa cách với mọi người. Họ tin vào định mệnh và tin rằng số phận sẽ đem đến cho họ tình yêu đích thực. Tình cảm của họ rất mãnh liệt mặc dù họ không biết biểu lộ chúng bằng lời nói.