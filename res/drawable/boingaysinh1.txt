﻿	Số 1

Số 1 là những người rất tham vọng và độc lập, có cá tính mạnh. Họ có nhiều sáng kiến, táo bạo và không sợ khó khăn. Sứ mệnh của họ là truyền nhiệt huyết cho người khác. Dối với những người số 1 thì không có điều gì là không thể. Họ muốn người khác đi theo vết chân của mình. Không ai thay thế được họ ở nhũng nơi mà mọi người cần được tiếp thêm sức mạnh, cần giữ vững tinh thần, cần những lời khuyên. Những ý tưởng của họ nhiều khi rất bất ngờ, nhưng họ không bao giờ gặp khó khăn khi tìm những người thực hiện các ý tưởng thậm chí là điên rồ nhất.

Số 1 là các nhà lãnh đạo bẩm sinh, họ tự hào về thành công của mình và biết giá trị của bản thân. Họ không thích làm theo lệnh người khác, kỷ luật khắt khe hay các lệnh cấm thường khiến họ nổi loạn. Vì thế khi còn trẻ họ có thể gặp khó khăn trong việc tuân thủ các quy tắc xã hội.
Họ hoàn toàn không tự ti và do đó thường bị coi là những người kiêu căng và hiếu thắng. Họ cũng hay nóng vội, nhưng họ tin tưởng sâu sắc vào ngôi sao may mắn của mình.

Người số 1 hay di chuyển. Họ thay đổi nơi ở và nơi làm việc luôn. Họ không chịu nổi sự buồn chán, lúc nào cũng phải làm cái gì đó. Họ thích những nơi vui vẻ, nhưng không ưa ngồi lê dôi mách. Lanh lợi và khéo ăn nói, họ biết sử dụng những ưu điểm này của mình khi xung đột. Những lý lẽ thuyêt phục được họ phải là nhũng lý luận lô gích.

Trong tình bạn cũng như tình yêu họ luôn muốn là người quyết định. Họ không để ý mấy đến cảm nhận của người khác. Trong vấn đề tài chính họ hay mạo hiểm, nhưng lại rất may mắn. Họ tiêu pha nhiều, thích mua những đồ đắt tiền de lux và những chiếc xe thể thao.
Trong tình yêu họ thích chinh phục. Họ rất quyến rũ và được nhiều người thích. Nhưng cần rất thận trọng vì sự áp đảo của họ đối với bạn đời có thể dẫn đến đổ vỡ.